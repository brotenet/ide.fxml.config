package ide.fxml.config;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.json.JSONObject;
import org.json.util.converters.XMLConverter;

public class JarSelectDialog extends Dialog {

	protected Object output;
	protected Shell shell;
	private String project_path;
	private List list;
	
	/**
	 * Create the dialog.
	 * @param parent
	 * @param style
	 */
	public JarSelectDialog(Shell parent, String project_path) {
		super(parent, SWT.DIALOG_TRIM | SWT.RESIZE | SWT.APPLICATION_MODAL);
		this.project_path = project_path;
		setText("SWT Dialog");
	}

	/**
	 * Open the dialog.
	 * @return the result
	 */
	public Object open() {
		createContents();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return output;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		
		
		
		shell = new Shell(getParent(), getStyle());
		shell.setImage(new Image(null, JarSelectDialog.class.getResourceAsStream("/ide/fxml/config/jar.png")));
		shell.setSize(450, 300);
		shell.setText("Select .Jar libraries");
		shell.setLayout(new GridLayout(2, false));
		
		Label lblNewLabel = new Label(shell, SWT.NONE);
		lblNewLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		lblNewLabel.setText("Select .Jar libraries containing JavaFX control classes\nto be included in configuration references.");
		
		list = new List(shell, SWT.BORDER | SWT.V_SCROLL | SWT.MULTI);
		list.setItems(new String[] {});
		list.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		
		Button btnCancel = new Button(shell, SWT.NONE);
		btnCancel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				output = null;
				shell.close();
			}
		});
		btnCancel.setImage(new Image(null, JarSelectDialog.class.getResourceAsStream("/ide/fxml/config/delete.png")));
		GridData gd_btnCancel = new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1);
		gd_btnCancel.widthHint = 100;
		btnCancel.setLayoutData(gd_btnCancel);
		btnCancel.setText("Cancel");
		
		Button btnApply = new Button(shell, SWT.NONE);
		btnApply.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				output = list.getSelection();
				shell.close();
			}
		});
		btnApply.setImage(new Image(null, JarSelectDialog.class.getResourceAsStream("/ide/fxml/config/update.png")));
		GridData gd_btnApply = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_btnApply.widthHint = 100;
		btnApply.setLayoutData(gd_btnApply);
		btnApply.setText("Apply");
		
		populate();

	}
	
	private void populate() {
		for (JSONObject entry : XMLConverter.toJSONObject(readTextFile(project_path + "/.classpath")).getJSONObject("classpath").getJSONArray("classpathentry").toArray(JSONObject.class)) {
			if(entry.getString("kind").equalsIgnoreCase("lib")) {
				list.add("/" + entry.getString("path"));
			}			
		}
	}
	
	private static String readTextFile(String file_path) 
	{
		StringBuilder contentBuilder = new StringBuilder();
		try (Stream<String> stream = Files.lines(Paths.get(file_path), StandardCharsets.UTF_8)) 
		{
			stream.forEach(s -> contentBuilder.append(s).append("\n"));
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		return contentBuilder.toString();
	}

}
