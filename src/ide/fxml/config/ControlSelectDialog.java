package ide.fxml.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.json.JSONObject;

public class ControlSelectDialog extends Dialog {

	protected Object output;
	protected Shell shell;
	private Text txt_description;
	private List list_classes;
	private List list_images;
	private Combo txt_category;
	private String library_path;
	private String[] descriptions;
	private String[] categories;
	private Label lbl_message;
	
	/**
	 * Create the dialog.
	 * @param parent
	 * @param style
	 */
	public ControlSelectDialog(Shell parent, String library_path, String[] descriptions, String[] categories) {
		super(parent, SWT.DIALOG_TRIM | SWT.RESIZE | SWT.APPLICATION_MODAL);
		this.library_path = library_path;
		this.categories = categories;
		this.descriptions = descriptions;
		setText("SWT Dialog");
	}

	/**
	 * Open the dialog.
	 * @return the result
	 */
	public Object open() {
		createContents();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return output;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), getStyle());
		shell.setImage(new Image(null, ControlSelectDialog.class.getResourceAsStream("/ide/fxml/config/component.png")));
		shell.setSize(635, 440);
		shell.setText("Select JavaFX controls");
		shell.setLayout(new GridLayout(2, false));
		
		lbl_message = new Label(shell, SWT.NONE);
		lbl_message.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
		lbl_message.setText("Select JavaFX control and image from library #LIB#\nand set the category and description to be used by the FXML editor.");
		
		Composite composite_1 = new Composite(shell, SWT.NONE);
		composite_1.setLayout(new GridLayout(2, false));
		composite_1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		
		Group grpClassReferences = new Group(composite_1, SWT.NONE);
		grpClassReferences.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		grpClassReferences.setText("Class References:");
		grpClassReferences.setLayout(new GridLayout(1, false));
		
		list_classes = new List(grpClassReferences, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		list_classes.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		Group grpImageReferences = new Group(composite_1, SWT.NONE);
		grpImageReferences.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		grpImageReferences.setText("Image References:");
		grpImageReferences.setLayout(new GridLayout(1, false));
		
		list_images = new List(grpImageReferences, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		list_images.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		Composite composite = new Composite(shell, SWT.NONE);
		composite.setLayout(new GridLayout(2, false));
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
		
		Label lblNewLabel = new Label(composite, SWT.NONE);
		lblNewLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblNewLabel.setText("Category:");
		
		txt_category = new Combo(composite, SWT.NONE);
		txt_category.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		Label lblNewLabel_1 = new Label(composite, SWT.NONE);
		lblNewLabel_1.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblNewLabel_1.setText("Description:");
		
		txt_description = new Text(composite, SWT.BORDER);
		txt_description.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		Button btnCancel = new Button(shell, SWT.NONE);
		btnCancel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				shell.close();
			}
		});
		GridData gd_btnCancel = new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1);
		gd_btnCancel.widthHint = 100;
		btnCancel.setLayoutData(gd_btnCancel);
		btnCancel.setText("Cancel");
		btnCancel.setImage(new Image(null, ControlSelectDialog.class.getResourceAsStream("/ide/fxml/config/delete.png")));
		
		Button btnApply = new Button(shell, SWT.NONE);
		btnApply.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				JSONObject selection = new JSONObject();
				if(txt_category.getText().trim().length() > 0 && txt_description.getText().trim().length() > 0) {
					if(!Arrays.asList(descriptions).contains(txt_category.getText().trim())) {
						JSONObject reference = new JSONObject();
						reference.put("description", txt_description.getText().trim());
						reference.put("category", txt_category.getText().trim());
						reference.put("class_path", list_classes.getSelection()[0].trim());
						reference.put("image", list_images.getSelection()[0].trim());
						output = reference;
						shell.close();
					}else {
						MessageDialog.openError(shell, "Invalid input", "Description field must have a unique value, not contained in the existing class references.");
					}					
				}else {
					MessageDialog.openError(shell, "Invalid input", "Category and Description fields must not be empty.");
				}				
			}
		});
		GridData gd_btnApply = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_btnApply.widthHint = 100;
		btnApply.setLayoutData(gd_btnApply);
		btnApply.setText("Apply");
		btnApply.setImage(new Image(null, ControlSelectDialog.class.getResourceAsStream("/ide/fxml/config/update.png")));

		try {
			populate();
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	
	private void populate() throws IOException {
		String lib_name = library_path.split("/")[library_path.split("/").length-1].trim();
		lib_name = lib_name.substring(0,  lib_name.length()-4);
		lbl_message.setText(lbl_message.getText().replace("#LIB#", lib_name));
		if(categories != null) {
			txt_category.setItems(categories);
		}
		@SuppressWarnings("resource")
		ZipInputStream zip = new ZipInputStream(new FileInputStream(library_path));
	    ZipEntry ze = null;
	    while( ( ze = zip.getNextEntry() ) != null ) {
	        String entryName = ze.getName();
	        if( !entryName.contains("$")) {
	        	if(entryName.endsWith(".class")) {
	        		entryName = entryName.substring(0, entryName.length()-6).replace("/", ".");
	        		list_classes.add( entryName  );
	        	}else if(entryName.endsWith(".png")) {
	        		list_images.add( entryName  );
	        	}
	        }
	    }
	    list_classes.setSelection(0);
	    list_images.setSelection(0);
	}

}
