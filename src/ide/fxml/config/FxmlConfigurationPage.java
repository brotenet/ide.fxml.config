package ide.fxml.config;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;
import org.json.JSONArray;
import org.json.JSONObject;

public class FxmlConfigurationPage extends Composite {

	private JSONObject configuration;
	private String project_path;
	private String file_path;
	private Tree tree_lib_refs;
	private Tree tree_ctl_refs;
	
	
	public FxmlConfigurationPage(Composite parent, JSONObject configuration, String file_path) {
		super(parent, SWT.NONE);
		this.configuration = configuration;
		this.file_path = file_path;
		this.project_path = Activator.getProjectPath().substring(0, Activator.getProjectPath().length()-1);
		
		setBackground(new Color(null, 255, 255, 255));
		GridLayout gridLayout = new GridLayout(2, false);
		gridLayout.verticalSpacing = 15;
		gridLayout.marginBottom = 15;
		gridLayout.marginTop = 15;
		gridLayout.marginRight = 15;
		gridLayout.marginLeft = 15;
		gridLayout.horizontalSpacing = 15;
		setLayout(gridLayout);
		
		Composite composite_7 = new Composite(this, SWT.BORDER);
		composite_7.setBackground(new Color(null, 220, 220, 220));
		composite_7.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
		composite_7.setLayout(new GridLayout(3, false));
		
		Label lblNewLabel_1 = new Label(composite_7, SWT.NONE);
		lblNewLabel_1.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		
		Button btnUpdate = new Button(composite_7, SWT.NONE);
		btnUpdate.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(MessageDialog.openConfirm(getShell(), "Confirm update", "Please confirm that you wish to update references and override the existing configuration.") == true) {
					JSONArray libaries = new JSONArray();
					for(TreeItem item : tree_lib_refs.getItems()) {
						libaries.put(((String) item.getData("reference")).trim());
					}
					JSONArray controls = new JSONArray();
					for(TreeItem item : tree_ctl_refs.getItems()) {
						JSONObject reference = new JSONObject();
						reference.put("description", item.getData("description"));
						reference.put("category", item.getData("category"));
						reference.put("class_path", item.getData("class_path"));
						reference.put("image", item.getData("image"));
						controls.put(reference);
					}
					configuration.put("libraries", libaries);
					configuration.put("controls", controls);
					try {
						BufferedWriter writer = new BufferedWriter(new FileWriter(file_path));
					    writer.write(configuration.toString(1));
					    writer.close();
					} catch (Exception e2) {
						e2.printStackTrace();
					}
					refresh();
				}
			}
		});
		btnUpdate.setToolTipText("Update configuration with current changes");
		btnUpdate.setImage(new Image(null, FxmlConfigurationPage.class.getResourceAsStream("/ide/fxml/config/update.png")));
		btnUpdate.setText("Update");
		
		Button btnReset = new Button(composite_7, SWT.NONE);
		btnReset.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(MessageDialog.openConfirm(getShell(), "Confirm reset", "Please confirm that you wish to reset all reference changes.") == true) {
					populate();
				}
			}
		});
		btnReset.setToolTipText("Revert to previous configuration");
		btnReset.setImage(new Image(null, FxmlConfigurationPage.class.getResourceAsStream("/ide/fxml/config/undo.png")));
		btnReset.setText("Reset");
		
		Composite composite = new Composite(this, SWT.BORDER);
		composite.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, false, true, 1, 1));
		composite.setBackground(new Color(null, 255, 255, 255));
		GridLayout gl_composite = new GridLayout(1, false);
		gl_composite.marginWidth = 0;
		gl_composite.verticalSpacing = 0;
		gl_composite.marginHeight = 0;
		gl_composite.horizontalSpacing = 0;
		composite.setLayout(gl_composite);
		
		Composite composite_1 = new Composite(composite, SWT.NONE);
		composite_1.setBackground(new Color(null, 220, 220, 220));
		composite_1.setLayout(new GridLayout(3, false));
		composite_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		CLabel lblNewLabel = new CLabel(composite_1, SWT.NONE);
		lblNewLabel.setBackground(new Color(null, 220, 220, 220));
		lblNewLabel.setImage(new Image(null, FxmlConfigurationPage.class.getResourceAsStream("/ide/fxml/config/jar.png")));
		lblNewLabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		lblNewLabel.setText("Project Library References");
		
		Button btn_lib_ref_add = new Button(composite_1, SWT.NONE);
		btn_lib_ref_add.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				JarSelectDialog dialog = new JarSelectDialog(getShell(), project_path);
				String[] selections = (String[]) dialog.open();
				if(selections != null) {
					for(String selection : selections) {
						boolean insert = true;
						for(TreeItem item : tree_lib_refs.getItems()) {
							if(selection.trim().equalsIgnoreCase(((String)item.getData("reference")).trim())) {
								insert = false;
							}
						}
						if(insert == true) {
							TreeItem item = new TreeItem(tree_lib_refs, SWT.NONE);
							item.setText(selection.split("/")[selection.split("/").length-1]);
							item.setData("reference", selection);
							item.setData("path", project_path + selection);
						}
					}
					pack(tree_lib_refs);
				}
			}
		});
		btn_lib_ref_add.setToolTipText("Add new reference");
		btn_lib_ref_add.setImage(new Image(null, FxmlConfigurationPage.class.getResourceAsStream("/ide/fxml/config/add.png")));
		btn_lib_ref_add.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		
		Button btn_lib_ref_delete = new Button(composite_1, SWT.NONE);
		btn_lib_ref_delete.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(MessageDialog.openConfirm(getShell(), "Confirm deletion", "Please confirm that you wish to delete the selected reference.") == true) {
					tree_lib_refs.getSelection()[0].dispose();
				}
			}
		});
		btn_lib_ref_delete.setToolTipText("Remove selected reference");
		btn_lib_ref_delete.setImage(new Image(null, FxmlConfigurationPage.class.getResourceAsStream("/ide/fxml/config/delete.png")));
		
		Composite composite_2 = new Composite(composite, SWT.NONE);
		composite_2.setBackground(new Color(null, 220, 220, 220));
		composite_2.setLayout(new GridLayout(1, false));
		composite_2.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		composite_2.setBounds(0, 0, 64, 64);
		
		tree_lib_refs = new Tree(composite_2, SWT.BORDER | SWT.FULL_SELECTION);
		tree_lib_refs.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		tree_lib_refs.setLinesVisible(true);
		tree_lib_refs.setBounds(0, 0, 80, 80);
		
		Composite composite_3 = new Composite(this, SWT.BORDER);
		composite_3.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		composite_3.setBackground(new Color(null, 220, 220, 220));
		GridLayout gl_composite_3 = new GridLayout(1, false);
		gl_composite_3.verticalSpacing = 0;
		gl_composite_3.marginWidth = 0;
		gl_composite_3.marginHeight = 0;
		gl_composite_3.horizontalSpacing = 0;
		composite_3.setLayout(gl_composite_3);
		
		Composite composite_4 = new Composite(composite_3, SWT.NONE);
		composite_4.setBackground(new Color(null, 220, 220, 220));
		composite_4.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		composite_4.setLayout(new GridLayout(3, false));
		
		CLabel lblFxComponentReferences = new CLabel(composite_4, SWT.NONE);
		lblFxComponentReferences.setBackground(new Color(null, 220, 220, 220));
		lblFxComponentReferences.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		lblFxComponentReferences.setText("JavaFX Control Class References");
		lblFxComponentReferences.setImage(new Image(null, FxmlConfigurationPage.class.getResourceAsStream("/ide/fxml/config/component.png")));
		
		Button button = new Button(composite_4, SWT.NONE);
		button.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				if(tree_lib_refs.getSelection().length > 0) {
					String library_path = (String) tree_lib_refs.getSelection()[0].getData("path");
					ArrayList<String> descriptions = new ArrayList<>();
					ArrayList<String> categories = new ArrayList<>();
					if(tree_ctl_refs.getItems() != null) {
						for(TreeItem item : tree_ctl_refs.getItems()) {
							if(!descriptions.contains(item.getData("description"))) {
								descriptions.add((String) item.getData("description"));
							}
							if(!categories.contains(item.getData("category"))) {
								categories.add((String) item.getData("category"));
							}
						}
					}
					ControlSelectDialog dialog = new ControlSelectDialog(getShell(), library_path, descriptions.toArray(new String[descriptions.size()]), categories.toArray(new String[categories.size()]));
					JSONObject reference = (JSONObject) dialog.open();
					if(reference.has("description") && reference.has("category") && reference.has("class_path") && reference.has("image")) {
						TreeItem item = new TreeItem(tree_ctl_refs, SWT.NONE);
						item.setText(new String[] {reference.getString("description"), reference.getString("category"), reference.getString("class_path"), reference.getString("image")});
						item.setData("description", reference.getString("description"));
						item.setData("category", reference.getString("category"));
						item.setData("class_path", reference.getString("class_path"));
						item.setData("image", reference.getString("image"));
					}
					
				}else {
					MessageDialog.openInformation(getShell(), "No library Selected", "Please select a library reference before adding a new control reference.");
				}
			}
		});
		button.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		button.setToolTipText("Add new reference");
		button.setImage(new Image(null, FxmlConfigurationPage.class.getResourceAsStream("/ide/fxml/config/add.png")));
		
		Button button_1 = new Button(composite_4, SWT.NONE);
		button_1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(MessageDialog.openConfirm(getShell(), "Confirm deletion", "Please confirm that you wish to delete the selected reference.") == true) {
					tree_ctl_refs.getSelection()[0].dispose();
				}
			}
		});
		button_1.setToolTipText("Remove selected reference");
		button_1.setImage(new Image(null, FxmlConfigurationPage.class.getResourceAsStream("/ide/fxml/config/delete.png")));
		
		Composite composite_5 = new Composite(composite_3, SWT.NONE);
		composite_5.setBackground(new Color(null, 220, 220, 220));
		composite_5.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		composite_5.setLayout(new GridLayout(1, false));
		
		tree_ctl_refs = new Tree(composite_5, SWT.BORDER | SWT.FULL_SELECTION);
		tree_ctl_refs.setHeaderVisible(true);
		tree_ctl_refs.setLinesVisible(true);
		tree_ctl_refs.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		TreeColumn tcl_description = new TreeColumn(tree_ctl_refs, SWT.NONE);
		tcl_description.setWidth(100);
		tcl_description.setText("Description");
		
		TreeColumn tcl_category = new TreeColumn(tree_ctl_refs, SWT.NONE);
		tcl_category.setWidth(100);
		tcl_category.setText("Category");
		
		TreeColumn tcl_class_path = new TreeColumn(tree_ctl_refs, SWT.NONE);
		tcl_class_path.setWidth(100);
		tcl_class_path.setText("Class Path");
		
		TreeColumn tcl_image = new TreeColumn(tree_ctl_refs, SWT.NONE);
		tcl_image.setWidth(100);
		tcl_image.setText("Image Path");
		
	}

	public JSONObject getConfiguration() {
		return configuration;
	}

	public void setConfiguration(JSONObject configuration) {
		this.configuration = configuration;
	}

	public String getProjectPath() {
		return project_path;
	}

	public void setProjectPath(String project_path) {
		this.project_path = project_path;
	}

	public void populate() {
		tree_lib_refs.removeAll();
		tree_ctl_refs.removeAll();
		for(String reference : configuration.getJSONArray("libraries").toArray(String.class)) {
			TreeItem item = new TreeItem(tree_lib_refs, SWT.NONE);
			item.setText(reference.split("/")[reference.split("/").length-1]);
			item.setData("reference", reference);
			item.setData("path", project_path + reference);
			pack(tree_lib_refs);
		}
		for(JSONObject reference : configuration.getJSONArray("controls").toArray(JSONObject.class)) {
			TreeItem item = new TreeItem(tree_ctl_refs, SWT.NONE);
			item.setText(new String[] {reference.getString("description"), reference.getString("category"), reference.getString("class_path"), reference.getString("image")});
			item.setData("description", reference.getString("description"));
			item.setData("category", reference.getString("category"));
			item.setData("class_path", reference.getString("class_path"));
			item.setData("image", reference.getString("image"));
			pack(tree_ctl_refs);
		}
	}
	
	private static void pack(TreeColumn column)
	{
	    column.pack();
	    column.setWidth(column.getWidth() + 20);
	}
	
	private static void pack(Tree tree)
	{
	    for (TreeColumn tc : tree.getColumns())
	        pack(tc);
	}
	
	private void refresh() {
		try {
			Activator.getSelectedProject().refreshLocal(IResource.DEPTH_ZERO, null);
		} catch (CoreException e) {
			
		}
	}
	
	@Override
	protected void checkSubclass() {}
}
