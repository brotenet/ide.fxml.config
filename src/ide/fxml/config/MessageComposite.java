package ide.fxml.config;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

public class MessageComposite extends Composite {

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public MessageComposite(Composite parent, String title, String message) {
		super(parent, SWT.NONE);
		setLayout(new GridLayout(3, false));
		new Label(this, SWT.NONE);
		
		Label lblNewLabel_7 = new Label(this, SWT.NONE);
		lblNewLabel_7.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1));
		new Label(this, SWT.NONE);
		
		Label lblNewLabel_1 = new Label(this, SWT.NONE);
		lblNewLabel_1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		
		Composite composite = new Composite(this, SWT.BORDER);
		composite.setBackground(new Color(null, 255, 255, 255));
		GridLayout gl_composite = new GridLayout(1, false);
		gl_composite.marginWidth = 0;
		gl_composite.marginHeight = 0;
		composite.setLayout(gl_composite);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		
		Composite composite_1 = new Composite(composite, SWT.NONE);
		composite_1.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_DARK_GRAY));
		GridData gd_composite_1 = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
		gd_composite_1.widthHint = 300;
		composite_1.setLayoutData(gd_composite_1);
		GridLayout gl_composite_1 = new GridLayout(1, false);
		gl_composite_1.marginLeft = 5;
		gl_composite_1.horizontalSpacing = 0;
		gl_composite_1.marginHeight = 0;
		gl_composite_1.verticalSpacing = 0;
		gl_composite_1.marginWidth = 0;
		composite_1.setLayout(gl_composite_1);
		
		CLabel txt_tittle = new CLabel(composite_1, SWT.NONE);
		txt_tittle.setImage(new Image(null, MessageComposite.class.getResourceAsStream("/ide/fxml/config/warning_16.png")));
		txt_tittle.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		txt_tittle.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_DARK_GRAY));
		txt_tittle.setForeground(new Color(null, 255, 255, 255));
		txt_tittle.setBounds(0, 0, 64, 23);
		txt_tittle.setText("");
		
		Composite composite_2 = new Composite(composite, SWT.NONE);
		composite_2.setBackground(new Color(null, 255, 255, 255));
		composite_2.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		composite_2.setLayout(new GridLayout(2, false));
		
		Label lblNewLabel_2 = new Label(composite_2, SWT.NONE);
		lblNewLabel_2.setImage(new Image(null, MessageComposite.class.getResourceAsStream("/ide/fxml/config/warning_64.png")));
		
		CLabel txt_message = new CLabel(composite_2, SWT.NONE);
		txt_message.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
		txt_message.setBackground(new Color(null, 255, 255, 255));
		txt_message.setBounds(0, 0, 64, 23);
		txt_message.setText("");
		
		Label lblNewLabel_5 = new Label(this, SWT.NONE);
		lblNewLabel_5.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		new Label(this, SWT.NONE);
		
		Label lblNewLabel_3 = new Label(this, SWT.NONE);
		lblNewLabel_3.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1));
		new Label(this, SWT.NONE);

		txt_message.setText(message);
		txt_tittle.setText(title);
	}

	@Override
	protected void checkSubclass() {}
}
