package ide.fxml.config;


import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.EditorPart;
import org.json.JSONObject;


public class FxmlConfigurationController extends EditorPart implements IResourceChangeListener{
	public FxmlConfigurationController() {
	}

	FxmlConfigurationPage page;
	JSONObject configuration;
	JSONObject configuration_backup;

	
	@Override
	public void createPartControl(Composite parent) {
		configuration = new JSONObject(readTextFile(Activator.getEditorFile(this).getLocation().toString()));
        String project_path_prefix = Activator.getProjectPath();
        if(project_path_prefix.trim().length() > 0) {
        	page = new FxmlConfigurationPage(parent, configuration, Activator.getEditorFile(this).getLocation().toString());
        	page.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        	page.populate();
        }else {
        	MessageComposite message = new MessageComposite(
        			parent, 
        			"No active project",
        			"No project is currently selected in the Eclipse Project Explorer.\n"
        			+ "Please close this configuration editor and re-open the configuration\n"
        			+ "file in order to activate the project."
        		);        	
        	message.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        }
	}
	
	@Override
	public void resourceChanged(IResourceChangeEvent event) {
		
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doSaveAs() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		setSite(site);
		setInput(input);		
	}

	@Override
	public boolean isDirty() {
		return false;
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}
	
	private static String readTextFile(String file_path) 
	{
		StringBuilder contentBuilder = new StringBuilder();
		try (Stream<String> stream = Files.lines(Paths.get(file_path), StandardCharsets.UTF_8)) 
		{
			stream.forEach(s -> contentBuilder.append(s).append("\n"));
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		return contentBuilder.toString();
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
		
	}
}
